package com.pawelbanasik;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.pawelbanasik.domain.Organization;

public class BeanScopeApp {
	public static void main(String[] args) {

		ApplicationContext context = new FileSystemXmlApplicationContext("beans.xml");

		Organization organization = (Organization) context.getBean("myorg");
		Organization organization2 = (Organization) context.getBean("myorg");
		organization2.setPostalCode("98989");

		// System.out.println(organization.corporateSlogan());

		System.out.println(organization);
		System.out.println(organization2);
		
		// it will be true if objects are ideantical in memory only (works different then
		// equals method which comapares)
		if (organization == organization2) {
			System.out.println("Singleton scope test: organization and organization2" +
		" point to the same instance.");
		} else {
			System.out.println("Both organization and organization2 are seperate instances.");
		}
			
		// System.out.println(organization.corporateService());

		((AbstractApplicationContext) context).close();

	}
}
